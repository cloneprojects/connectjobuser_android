package com.app.connectJobUser.Volley;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.connectJobUser.R;
import com.app.connectJobUser.activities.AppSettings;
import com.app.connectJobUser.activities.SignInActivity;
import com.app.connectJobUser.application.Uberdoo;
import com.app.connectJobUser.helpers.Callback;
import com.app.connectJobUser.helpers.SharedHelper;
import com.app.connectJobUser.helpers.UrlHelper;
import com.app.connectJobUser.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by user on 23-10-2017.
 */

public class ApiCall {


    public static String strAdd;
    private static final int MY_SOCKET_TIMEOUT_MS = 50000;
    private static String TAG = ApiCall.class.getSimpleName();

    public static void getMethod(final Context context, final String url, final VolleyCallback volleyCallback) {

        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);

                        Utils.dismiss();
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.d(TAG, "onErrorResponse: " + url + ",error:" + error);
                VolleyErrorHandler.handle(url, error);


            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void getMethodHeaders(final Context context, final String url, final VolleyCallback volleyCallback) {

        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.d(TAG, "onErrorResponse: " + url + ",error:" + error);
                VolleyErrorHandler.handle(url, error);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                AppSettings appSettings = new AppSettings(context);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE, final Callback callback) {


        String lat = String.valueOf(LATITUDE);
        String lngg = String.valueOf(LONGITUDE);

        final String resultt = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + context.getResources().getString(R.string.google_maps_key);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, resultt, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d(TAG, "onResponse: " + jsonObject);
                Log.d(TAG, "resultUrl: " + resultt);

                callback.onSuccess(jsonObject);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("getjson", "onResponse: " + error);

            }
        });

        /*jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });*/

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Uberdoo.getInstance().addToRequestQueue(jsonObjectRequest);

    }


    public static void PostMethod(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        Utils.show(context);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("error_message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();

                VolleyErrorHandler.handle(url, error);
            }
        });
      /*  jsonObjReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });*/


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void PostMethodHeaders(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.show(context);
        Utils.log(TAG, "url:" + url + ",input: " + params);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("error_message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + url + ",error:" + error);
                Utils.dismiss();
                Log.d(TAG, "onErrorResponse: " + url + ",error:" + error);
                VolleyErrorHandler.handle(url, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                AppSettings appSettings = new AppSettings(context);
                Log.d(TAG, "getHeaders: " + appSettings.getToken());

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());
                return headers;
            }
        };
/*//        ;
        jsonObjReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });*/

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void PostMethodHeadersNoProgress(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("error_message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + url + ",error:" + error);
                VolleyErrorHandler.handle(url, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                AppSettings appSettings = new AppSettings(context);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());
                return headers;
            }
        };
//        ;
//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static void uploadImage(File file, Context context, final VolleyCallback volleyCallback) {
        Utils.show(context);
//
//        Ion.with(context)
//                .load("POST", UrlHelper.UPLOAD_IMAGE)
//                .setMultipartFile("file", file)
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e,
//                                            com.koushikdutta.ion.Response<String> result) {
//                        if (e == null) {
//                            try {
//                                JSONObject jsonObject=new JSONObject(result.getResult());
//                                volleyCallback.onSuccess(jsonObject);
//
//                            } catch (JSONException e1) {
//
//                            }
//
//                            Log.d(TAG, "onCompleted: " + result.getResult());
//                        } else {
//                            e.printStackTrace();
//
//                        }
//                    }
//                });
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", "fileName", RequestBody.create(MEDIA_TYPE_PNG, new File(file.getPath())))
                .build();
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(UrlHelper.UPLOAD_IMAGE + SharedHelper.getKey(context, "getLanguage"))
                .post(requestBody).build();

        try {
            okhttp3.Response response = client.newCall(request).execute();
            JSONObject jsonObject = new JSONObject(response.body().string());
            Utils.dismiss();
            volleyCallback.onSuccess(jsonObject);

        } catch (IOException e) {
            Log.e(TAG, "upload_the_image_error:io " + e);
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(TAG, "upload_the_image_error:json " + e);
            e.printStackTrace();
        }

    }

}