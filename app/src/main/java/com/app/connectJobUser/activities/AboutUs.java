package com.app.connectJobUser.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.connectJobUser.R;
import com.app.connectJobUser.Volley.ApiCall;
import com.app.connectJobUser.Volley.VolleyCallback;
import com.app.connectJobUser.helpers.SharedHelper;
import com.app.connectJobUser.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutUs extends BaseActivity {
    WebView webView;
    TextView textView;
    ImageView backButton;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
     //   webView = (WebView) findViewById(R.id.webView);
        textView = (TextView) findViewById(R.id.textView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiCall.PostMethod(AboutUs.this, UrlHelper.ABOUT_US
                + SharedHelper.getKey(AboutUs.this, "getLanguage"), new JSONObject(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray static_pages = response.optJSONArray("page");
                JSONObject jsonObject = static_pages.optJSONObject(0);
                String about = jsonObject.optString("privacyPolicy");
                textView.setText(about);

              /*  String url = jsonObject.optString("page_url");
                webView.getSettings().setJavaScriptEnabled(false);
                webView.loadUrl(url);
                webView.setHorizontalScrollBarEnabled(false);*/
            }
        });


    }
}
