package com.app.connectJobUser.activities;

/*
 * Created by admin on 16-07-2018.
 */

import android.support.v7.app.AppCompatActivity;

import com.app.connectJobUser.helpers.LocaleUtils;

public class BaseActivity extends AppCompatActivity {
    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }

}